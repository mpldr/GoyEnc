package yenc

// Yencify takes in a bytearray and returns the yEnc encoded bytearray
func Yencify(content []byte) []byte {
	result := []byte{}
	for _, r := range content {
		escape := false
		temp := int(r)

		// bin + 42d
		temp += 42

		// % 256d
		temp %= 256

		// if 00h 09h 0Ah 0Dh or 3Dh
		if temp == 0 || temp == 9 || temp == 10 || temp == 13 || temp == 46 || temp == 61 {
			// + 64d
			temp += 64

			// % 256d
			temp %= 256
			escape = true
		}

		if escape {
			result = append(result, '=', byte(temp))
		} else {
			result = append(result, byte(temp))
		}
	}

	return result
}

// UnYencify takes in a bytearray and returns the decoded bytes
func UnYencify(content []byte) []byte {
	result := []byte{}
	escaped := false

	for _, r := range content {
		if r == '=' && !escaped {
			escaped = true
			continue
		}
		temp := int(r)

		if escaped {
			// + 64d
			temp -= 64

			// % 256d
			temp %= 256
			escaped = false
		}
		// bin + 42d
		temp -= 42

		// % 256d
		temp %= 256

		result = append(result, byte(temp))
	}

	return result
}
