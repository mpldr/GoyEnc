# GoyEnc

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/poldi1405/GoyEnc)](https://goreportcard.com/report/gitlab.com/poldi1405/GoyEnc)
[![GoDoc](https://godoc.org/github.com/poldi1405/GoyEnc?status.svg)](https://godoc.org/github.com/poldi1405/GoyEnc)
[![Coverage Status](https://coveralls.io/repos/gitlab/poldi1405/GoyEnc/badge.svg?branch=master)](https://coveralls.io/gitlab/poldi1405/GoyEnc?branch=master)

yEnc Encoder according to [the official specification](http://www.yenc.org/yenc-draft.1.3.txt)

## ToDo

- [ ] Make it not kill your CPU and eat your RAM
	- [ ] enable configuration of number of goroutines
- [ ] add tests
- [ ] verify implementation
- [ ] add Documentation
- [ ] improve RAM & CPU Usage
