package yenc

import . "testing"

var maxGoroutines = int64(128)

func TestChunkMaximum(t *T) {
	lim, err := getLimit(2, 5)
	if err != nil || lim != 2 || lim < 1 || lim > maxGoroutines {
		t.Fail()
	}
}

func TestChunkTooBig(t *T) {
	_, err := getLimit(50, 123456789012)
	if err == nil {
		t.Fail()
	}
}

func TestRAMExceeded(t *T) {
	// TODO: verify this works on all systems
	lim, err := getLimit(128, 455556748)
	if err != nil || lim < 1 || lim > maxGoroutines {
		t.Fail()
	}
}

func TestALotOfChunks(t *T) {
	lim, err := getLimit(9999999, 50)
	if err != nil || lim > maxGoroutines {
		t.Fail()
	}
}
